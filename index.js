// alert("Hello Batch 224!");


/*
	OBJECTS
		-An object is a data type that is used to represents real world objects. It is also a collection of related data and/or functionalities.

	Object Literals
		- one of the methods in creating objects.

	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}


*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999

};

console.log("Result rom creating object using Object Literals: ");
console.log(cellphone);
console.log(typeof cellphone);

// Creating Objects using a Constructor Function (Object Constructor)
/*
	Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.
	

	Syntax:
		function ObjectName(valueA, valueB) {
			this.keyA = valueA;
			this.keyB = valueB

		};

		let variableName = new function ObjectName(valueA, valueB);
		console.log(variableName);

		===========================================================
			-this is for invoking; it refers to the global object.
			-dont't forget the "new" keyword when creating a new object.
*/
	
	// We use PascalCase for the constructor function name.
	// Template
function Laptop (name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate
};

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of Creating Objects using Constructor Function:");
console.log(laptop);

let mylaptop = new Laptop("MacBook Air", [2020, 2021]);
console.log(mylaptop);

let oldLaptop = Laptop("Portal RME CCMC", 1980);
console.log("Result of creating objects without the new keyword: ");
console.log(oldLaptop);


// Creating empty objects as placeholder;
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

myComputer = {
	name: "Asus",
	manufactureDate: 2012
};
console.log(myComputer);

/*Mini Activity:
Create an object constructor function to produce 2 objects with 3 key-value pairs.
Log the 2 new objects in the console and send SS in our batch hangout.*/

function Phone (name, manufactureDate, ram) {
	this.name = name;
	this.manufactureDate = manufactureDate;
	this.ram = ram
};

let cellPhone1 = new Phone("Samsung", 2010 , "2GB");
console.log(cellPhone1);

let cellPhone2 = new Phone("Realme", 2018 , "4GB");
console.log(cellPhone2);

let cellPhone3 = new Phone("Huawei", 2020 , "8GB");
console.log(cellPhone3);

// Accessing Object Properties

// Using the dot notation
console.log("Result from dot notation: " + mylaptop.name);

// Using the square bracket notation
console.log("Result from square bracket notation: " + mylaptop["name"]);

// Accessing array of objects
let deviceArr = [laptop, mylaptop];
// let deviceArr = [{name: Lenovo, manufactureDate: 2008}, {name: MacBook Air, manufactureDate: 2020}];

//Dot Notation
console.log(deviceArr[0].manufactureDate);

//Square Bracket Notation
console.log(deviceArr[0]["manufactureDate"]);


// Initializing/Adding/Deleting/Reassigning Object Properties
// (CRUD Operations)

//Initialize
let car = {};
console.log(car);

//Adding Object Properties
car.name = "Honda Civic";
console.log("Result from adding property using dot notation:");
console.log(car);

car["manufacture date"] = 2009;
console.log("Result from adding property using square bracket notation:");
console.log(car);

//Deleting object Properties
delete car["manufacture date"];
console.log("Result from deleting object properties:");
console.log(car);

// Reassigning object properties (update)
car.name = "Tesla";
console.log("Result from reasigning property:");
console.log(car);

// Object Methods

/*
	This method is a function which is stored in an object property. There are also function and one of the key difference that they have is that methods are functions related to a specific object.
*/

let person = {
	name: "John",
	age: 25,
	talk: function() {
		console.log("Hello! My Name is " + this.name);
	}
};

console.log(person);
console.log("Result from Object Method: ");
person.talk();

person.walk = function () {
	console.log(this.name + " have walked 25 steps forward.");
};

person.walk();


let friend = {
	firstName: "Jane",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["janedoe@mail.com", "jane121992@gmail.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " "+ this.lastName + "." + " I live in " + this.address.city + ", " + this.address.country + ".")
	}
};

friend.introduce();

// Real World Application og Ojects

// Using Object Literals

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 15,
	tackle: function() {
		console.log(this.name + " tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	},
	faint: function() {
		console.log(this.name + " fainted.")
	}
};

console.log(myPokemon);
myPokemon.tackle();

// Creating Real World Object Using Constructor Function
function Pokemon(name, level) {

	// Porpeties
	this.name = name
	this.level = level
	this.health = 5 * level
	this.attack = 2 * level

	// Methods
	this.tackle = function(target) {
		console.log(this.name +" tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		target.health = target.health -this.attack
		if(target.health <= 0 ) {
			target.faint()
		}
	},

	this.faint = function() {
		console.log(this.name + " fainted.")
	}
};

let charmander = new Pokemon("Charmander", 12);
let squirtle = new Pokemon("Squirtle", 6);

console.log(charmander);
console.log(squirtle);

charmander.tackle(squirtle);

/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 0, invoke faint function


*/

charmander.tackle(squirtle);
console.log(squirtle);
console.log("Squirtle Health: " + squirtle.health);